# frozen_string_literal: true

class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, with: :handle_not_found

  protect_from_forgery unless: :json_request? 

  def handle_not_found
    redirect_to root_path
  end

  protected

  def block_if_profile_blank!
    unless [current_user.name?, current_user.address?, current_user.phone_number?,
            current_user.avatar.attached?].all?(&:present?)
      respond_to do |format|
        format.html { redirect_to profiles_path(redirect_url: request.path) }
        format.json do
          @errors = { profile: ["is not completed"] }
          render "application/errors", status: :unprocessable_entity
        end
      end
    end
  end

  def perform_transaction(&)
    ApplicationRecord.transaction(&)
  end

  def json_request?
    request.format.json?
  end
end
