# frozen_string_literal: true

class ProductsController < ApplicationController
  before_action :set_product, only: %i[show edit update destroy]
  before_action :authenticate_user!, only: %i[new create edit update destroy]
  before_action :block_if_profile_blank!, only: %i[new create edit update destroy]
  before_action :verify_ownership, only: %i[edit update destroy]

  layout 'full'

  # GET /products or /products.json
  def index
    @products = Product.includes(:category).with_attached_images.published.paginate(page: params[:page].present? ? params[:page] : 1, per_page: 10)
    @products = @products.all if params[:product].try(:[], :category_id).blank?
    @products = @products.where(filter_params) if params[:product].present?

    respond_to do |format|
      format.html do
        authenticate_user!
        @products = @products.where(user: current_user)
      end

      format.json do
        @products = @products.includes(user: :city)

        if user_signed_in? && params[:user_id].present?
          @products = @products.where(user: current_user)
        end
      end
    end
  end

  # GET /products/1 or /products/1.json
  def show
    @offer = Offer.new unless mine?
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit; end

  # POST /products or /products.json
  def create
    @product = Product.new(product_params.merge(user_id: current_user.id))

    respond_to do |format|
      if @product.save
        @product.notify_user if @product.published?

        format.html { redirect_to product_url(@product), notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1 or /products/1.json
  def update
    unless params[:product][:preview]
      @product.images.each do |image|
        image.purge unless update_product_params[:persisted_images].try(:include?, image.try(:signed_id))
      end
    end

    respond_to do |format|
      if @product.update(update_product_params.except(:persisted_images, :images))
        @product.notify_user if @product.status_previously_changed? && @product.published?
        @product.images.attach(update_product_params[:images])

        format.html { redirect_to product_url(@product), notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1 or /products/1.json
  def destroy
    @product.destroy

    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_product
    @product = Product.with_attached_images.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :price, :description, :category_id, :status, images: [])
  end

  def update_product_params
    params.require(:product).permit(:name, :price, :description, :category_id, :status, images: [], persisted_images: [])
  end

  def filter_params
    params.require(:product).permit(:sold, :liked)
  end

  def verify_ownership
    return if mine?

    respond_to do |format|
      format.html { redirect_to products_url, status: :forbidden }
      format.json { render json: { "user_id": ["doesn't have the right to do this action"] }, status: :forbidden }
    end
  end

  def mine?
    user_signed_in? && @product.user_id.eql?(current_user.id)
  end
end
