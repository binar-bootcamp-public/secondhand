# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  protected

  def respond_with(resource, opts = {})
    respond_to do |format|
      format.html do
        super
      end

      format.json do
        render
      end
    end
  end
end
