json.extract! offer, :id, :price, :status, :created_at, :updated_at
json.product offer.product, partial: 'products/product', as: :product
json.to offer.to, partial: 'users/user', as: :user
json.from offer.from, partial: 'users/user', as: :user
