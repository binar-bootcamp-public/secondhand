# frozen_string_literal: true

json.partial! 'application/pagination', resources: @offers
json.offers @offers, partial: 'offers/offer', as: :offer
