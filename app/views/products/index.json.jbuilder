# frozen_string_literal: true

json.partial! 'application/pagination', resources: @products
json.products @products, partial: 'products/product', as: :product
