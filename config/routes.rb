# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  get 'categories/index'
  get 'categories/show'

  resources :products
  resources :offers, only: %i[create update]
  resources :categories, only: %i[index show], defaults: { format: :json }

  get 'users/:user_id/offers', to: 'offers#index', as: 'user_offers'

  get 'profiles', to: 'profiles#show'
  put 'profiles', to: 'profiles#update'

  resources :notifications, only: %[index] do
    put '/read', to: 'notifications#read', on: :member
  end

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
  }

  devise_scope :user do
    get "users", to: "devise/sessions#new"
  end

  root 'home#index'
end
