# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

samples = Dir[Rails.root.join('db/assets/sample*.png')]

ApplicationRecord.transaction do
  user = User.find_by(email: 'fnurhidayat@binar.co.id')
  unless user.present?
    city = City.find_by!(name: 'Solo')
    user = User.create!(name: 'Fikri',
                        email: 'fnurhidayat@binar.co.id',
                        password: '123456',
                        phone_number: '08123123123123',
                        address: Faker::Address.full_address,
                        city:)
    user.avatar.attach(io: Rails.root.join('db/assets/', 'avatar.png').open, filename: "#{user.name.parameterize}.png",
                       content_type: 'image/png')
  end

  categories = Category.all

  25.times do
    product = Product.create!(name: Faker::Commerce.product_name.split(' ').slice(1, 2).join(' '),
                              price: Faker::Commerce.price(range: 1000..200_000),
                              description: Faker::Lorem.paragraph,
                              status: :published,
                              category: categories.sample,
                              user:)
    product.images.attach(io: File.open(samples.sample), filename: "#{product.name.parameterize}.png",
                          content_type: 'image/png')
    product.notify_user
  end

  Notification.where(user_id: user.id).update_all read: true
end
